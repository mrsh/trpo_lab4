﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TRPO_Lab3Wpf
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public delegate void ResultUpdate();
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}
