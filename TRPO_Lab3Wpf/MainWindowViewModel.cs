﻿using System;
using System.Collections.Generic;
using System.Text;
using static TRPO_Lab3.Libr.Profit;

namespace TRPO_Lab3Wpf
{
    public class MainWindowViewModel : BaseViewModel
    {
        public event ResultUpdate UpdateResult;
        public MainWindowViewModel() { }
        private float _c;
        public float C
        {
            get { return _c; }
            set
            {
                    _c = value;
                    OnPropertyChanged();
                    UpdateResult();
            }
        }
        private float _b;
        public float B
        {
            get { return _b; }
            set
            {
                    _b = value;
                    OnPropertyChanged();
                    UpdateResult();
            }
        }
        private float _a;
        public float A
        {
            get { return _a; }
            set
            {
                    _a = value;                  
                    OnPropertyChanged();
                    UpdateResult();
            }
        }
        private float _result;
        public float Result
        {
            get { return _result; }
            set
            {
                    _result = value;
                    OnPropertyChanged();
            }
        }

        public void ResultUpdated()
        {
            Result = Gera(_a, _b, _c);
        }
    }
}
