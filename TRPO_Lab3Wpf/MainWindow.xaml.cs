﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TRPO_Lab3Wpf
{

    public partial class MainWindow 
    {
        public MainWindow()
        {           
            InitializeComponent();
            MainWindowViewModel mv = new MainWindowViewModel();
            DataContext = mv;
            mv.UpdateResult += mv.ResultUpdated;
        }

       
        
    }
}
